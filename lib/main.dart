import 'package:covid_app/profile.dart';
import 'package:covid_app/question.dart';
//import 'package:covid_app/risk.dart';
//import 'package:covid_app/vaccine.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'Depression Ckeck',
    home: MyApp(),
    theme: new ThemeData(scaffoldBackgroundColor: const Color(0xFF66FFFF)),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M';
  int age = 0;
  var questionScore = 0.0;
  var riskScore = 0.0;
  var vaccines = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('Name') ?? '';
      gender = prefs.getString('Gender') ?? 'M';
      age = prefs.getInt('Age') ?? 0;
      vaccines = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false,]';
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        if (arrStrQuestionValues[i].trim() == 'true') {
          questionScore++;
        }
      }
      questionScore = (questionScore * 100) / 9;
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('Name');
    prefs.remove('Gender');
    prefs.remove('Age');
    prefs.remove('question_values');
    await _loadProfile();
    await _loadQuestion();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Depression Ckeck',style: TextStyle(color: Colors.black),),
        backgroundColor: Color(0xFFFFCCFF),
      ),
      body: ListView(
        children: [
          Card(
            color: Colors.pink[50],
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text(name),
                  subtitle: Text(
                    'เพศ : $gender, อายุ: $age ปี',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'ความเสี่ยงที่จะเป็นโรคซึมเศร้า : ${questionScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (questionScore > 40)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 25,
                        color: (questionScore > 40)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.black.withOpacity(0.6)),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () async {
                        await _resetProfile();
                      },
                      child: const Text('รีเซ็ต'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text('โปรไฟล์'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('คำถามที่ใช้วัดความเป็นโรคซึมเศร้า'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => QuestionWidget()));
              await _loadQuestion();
            },
          ),
        ],
      ),
    );
  }
}
