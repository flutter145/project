import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questions = [
    '1.เบื่อทำอะไรก็ไม่เพลิดเพลิน',
    '2.ไม่สบายใจ ซึมเศร้า หรือท้อแท้',
    '3.หลับยากหรือหลับๆตื่น ๆหรือหลับมากไป',
    '4.เหนื่อยง่ายหรือไม่ค่อยมีแรง',
    '5.เบื่ออาหารหรือกินมากเกินไป',
    '6.รู้สึกไม่ดีกับตัวเอง คิดว่าตัวเองล้มเหลว',
    '7.สมาธิไม่ดีเวลาทำอะไร',
    '8.พูดหรือทำอะไรช้าจนคนอื่นมองเห็น',
    '9.คิดทำร้ายตนเองหรือฆ่าตัวตาย',
  ];
  var questionValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Question', style: TextStyle(color: Colors.black)),
          backgroundColor: Color(0xFFFFCCFF)),
      body: ListView(
        children: [
          CheckboxListTile(
              value: questionValues[0],
              title: Text(questions[0]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[0] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[1],
              title: Text(questions[1]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[1] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[2],
              title: Text(questions[2]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[2] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[3],
              title: Text(questions[3]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[3] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[4],
              title: Text(questions[4]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[4] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[5],
              title: Text(questions[5]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[5] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[6],
              title: Text(questions[6]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[6] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[7],
              title: Text(questions[7]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[7] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[8],
              title: Text(questions[8]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[8] = newvalue!;
                });
              }),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.pink,
                  onPrimary: Colors.white,
                  fixedSize: const Size(50, 40),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50))),
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('บันทึก'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false,]';
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        questionValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionValues.toString());
  }
}
